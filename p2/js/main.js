/**
 * CS4460 P2
 * Fall 2018
 *
 * Noah Roberts
*/


// ###############################
// SETUP
// ###############################

const margin = {top: 60, right: 25, bottom: 50, left: 60},
    width = 760 - margin.left - margin.right,
    height = 600 - margin.top - margin.bottom;
    centerPadding = 90;

const svg = d3.select("svg")
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .style("font-family", "Helvetica, sans-serif");

let x = d3.scaleBand()
    .range([0, width / 2 - centerPadding / 2])
    .padding(0.3);

let x2 = d3.scaleBand()
    .range([width / 2 + centerPadding / 2, width])
    .padding(0.3);

let y = d3.scaleLinear()
    .range([height, 0])
    .domain([0, 280000]);



// ###############################
// READ DATA
// ###############################

d3.csv("data/coffee_data.csv", function(error, data) {
    if (error) throw error;


    // ###############################
    // CHART 1: SALES BY REGION
    // ###############################

    // aggregate data
    let regionData = d3.nest()
        .key(function(d) { return d.region; })
        .rollup(function(v) {
            return {
                "length": v.length,
                "sum": d3.sum(v, function(d) { return +d.sales; })
            }
        })
        .entries(data);
    
    // map ordinal data to x axis
    x.domain(regionData.map(function(d) {
        return d.key;
    }));

    //bars
    svg.selectAll(".bar")
        .data(regionData)
        .enter().append("rect")
        .attr("x", function(d) {
            return x(d.key);
        })
        .attr("width", x.bandwidth())
        .attr("y", function(d) {
            return y(d.value.sum);
        })
        .attr("height", function(d) {
            return height - y(d.value.sum);
        })
        .style("fill", function(d) {
            if (d.key == "Central") {
                return "#2079b5";
            } else if (d.key == "East") {
                return "#ff800f";
            } else if (d.key == "South") {
                return "#2ba02d";
            } else if (d.key == "West") {
                return "#d52728";
            }
        });

    // axes
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    svg.append("text")             
        .attr("transform", "translate(" + ((width / 2 - centerPadding / 2) / 2) + "," + (height + 35) + ")")
        .style("text-anchor", "middle")
        .text("Region");

    svg.append("g")
        .call(d3.axisLeft(y)
            .tickFormat(d3.formatPrefix(".0", 1e3)));

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - 55)
        .attr("x",0 - (height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Coffee Sales (USD)");

    // title
    svg.append("text")             
        .attr("transform", "translate(" + ((width / 2 - centerPadding / 2) / 2) + "," + (0 - 20) + ")")
        .style("text-anchor", "middle")
        .text("Coffee Sales by Region (USD)")
        .style("font-size", "16px");



    // ###############################
    // CHART 2: SALES BY PRODUCT
    // ###############################

    // aggregate data
    let categoryData = d3.nest()
        .key(function(d) { return d.category; })
        .rollup(function(v) {
            return {
                "length": v.length,
                "sum": d3.sum(v, function(d) { return +d.sales; })
            }
        })
        .entries(data);
    
    // map ordinal data to x axis
    x2.domain(categoryData.map(function(d) {
        return d.key;
    }));

    // bars
    svg.selectAll(".bar")
        .data(categoryData)
        .enter().append("rect")
        .attr("x", function(d) {
            return x2(d.key);
        })
        .attr("width", x2.bandwidth())
        .attr("y", function(d) {
            return y(d.value.sum);
        })
        .attr("height", function(d) {
            return height - y(d.value.sum);
        })
        .style("fill", function(d) {
            if (d.key == "Coffee") {
                return "#4e3530";
            } else if (d.key == "Tea") {
                return "#cc291a";
            } else if (d.key == "Espresso") {
                return "#bd661f";
            } else if (d.key == "Herbal Tea") {
                return "#beb66d";
            }
        });

    // axes
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x2));

    svg.append("text")             
        .attr("transform", "translate(" + ((width / 2 + centerPadding / 2) + (width / 2 - centerPadding / 2) / 2) + "," + (height + 35) + ")")
        .style("text-anchor", "middle")
        .text("Product");

    svg.append("g")
        .attr("transform", "translate(" + (width / 2 + centerPadding / 2) + ",0)")
        .call(d3.axisLeft(y)
            .tickFormat(d3.formatPrefix(".0", 1e3)));

    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", width / 2 + centerPadding / 2 - 55)
        .attr("x",0 - (height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Coffee Sales (USD)");

    // title
    svg.append("text")             
        .attr("transform", "translate(" + ((width / 2 + centerPadding / 2) + (width / 2 - centerPadding / 2) / 2) + "," + (0 - 20) + ")")
        .style("text-anchor", "middle")
        .text("Coffee Sales by Product (USD)")
        .style("font-size", "16px");

    }
);
